import dearpygui.dearpygui as dpg

from trainerbase.gui import (
    add_codeinjection_to_gui,
    add_teleport_to_gui,
    simple_trainerbase_menu,
)

from injections import (
    no_reload,
    infinite_items,
    give_money_on_use,
    infinite_mana,
    give_adam_on_use,
    infinite_hp,
)
from teleport import tp


@simple_trainerbase_menu("BioShock Remastered", 600, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main", tag="code_injections"):
            add_codeinjection_to_gui(no_reload, "No reload", "F1")
            add_codeinjection_to_gui(infinite_items, "Infinite Items", "F2")
            add_codeinjection_to_gui(give_money_on_use, "Give Money On Use", "F3")
            add_codeinjection_to_gui(infinite_mana, "Infinite Mana", "F4")
            add_codeinjection_to_gui(infinite_hp, "Infinite HP", "F5")
            add_codeinjection_to_gui(give_adam_on_use, "Give Adam On Use", "F6")

        with dpg.tab(label="Teleport"):
            add_teleport_to_gui(tp, "Insert", "Home", "Delete")
