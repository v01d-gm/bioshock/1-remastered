from trainerbase.codeinjection import CodeInjection, AllocatingCodeInjection
from memory import bioshockhd, player_coords_pointer


infinite_items = AllocatingCodeInjection(
    bioshockhd.exe + 0x286EF6,
    """
        sub [ebp + 0x0C], eax
        mov ecx, 999
        mov [edx + 0x54], ecx
    """,
    original_code_length=8,
)

infinite_hp = AllocatingCodeInjection(
    bioshockhd.exe + 0x23CD0A,
    """
        push eax
        mov eax, 1000.0
        movd xmm0, eax       
        movss [esi + 0x57C],xmm0
        pop eax
    """,
    original_code_length= 8,
)

infinite_mana = CodeInjection(
    bioshockhd.exe + 0x1CF6CC,
    "nop\n" * 8,
)

give_money_on_use = AllocatingCodeInjection(
    bioshockhd.exe + 0x1CEFE7,
    """
    mov dword [esi + 0xAEC], 9999
    """,
    original_code_length=6,
)

give_adam_on_use = AllocatingCodeInjection(
    bioshockhd.exe + 0x1CF317,
    """
        mov dword [esi + 0xAF4], 9999
    """,
    original_code_length=6,
)

no_reload = CodeInjection(
    bioshockhd.exe + 0x72BE40,
    """
        nop
        nop
    """,
)

update_player_coords_pointer = AllocatingCodeInjection(
    bioshockhd.exe + 0x845137,
    f"""
        movss xmm0, [edx + 0x1D8]
        mov [{player_coords_pointer}], edx
    """,
    original_code_length=8,
)