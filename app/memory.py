from types import SimpleNamespace
from trainerbase.memory import POINTER_SIZE, pm


bioshockhd = SimpleNamespace(exe=pm.base_address)
player_coords_pointer = pm.allocate(POINTER_SIZE)
