from trainerbase.gameobject import GameFloat
from trainerbase.memory import Address

from memory import player_coords_pointer


player_coords_address = Address(player_coords_pointer, [0x1D8])
player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address.inherit(new_add=0x4))
player_z = GameFloat(player_coords_address.inherit(new_add=0x8))
